define(
    [
        'jquery',
        'underscore',
        'ko',
        'uiComponent',
        'uiRegistry',
        'Magento_Checkout/js/model/step-navigator',
        'Magento_Checkout/js/model/quote'
    ],
    function (
            $,
            _,
            ko,
            Component,
            registry,
            stepNavigator,
            quote
        ) {
        "use strict";
            
        var telephoneval = window.checkoutConfig.telephoneval;

        return Component.extend({
            defaults: {
                template: 'Magebees_Onepagecheckout/placeorder',
            },
            
            initialize: function () {
                $(function() {
                    /* For get the button inner html */
                    $('body').on("click", '.payment-method._active :input', function () {
                        $("#place-order-trigger").html($(".payment-method._active").find('.action.primary.checkout').html());
                    });
                });

                var self = this;
                this._super();

                return this;
            },
            isVisible: function () {
                return stepNavigator.isProcessed('shipping');
            },
            placeOrder: function () {
                var self = this;
                $(function() {
                    /* For triger place order button */
                    $('body').on("click", '#place-order-trigger', function (event) {
                    event.stopImmediatePropagation();
                    var errorMsg = "";
                    if (!quote.isVirtual()) {
                        if($(".magebees-checkout-shipping-address .shipping-address-items").length == 0){
                            if($.trim($('#co-shipping-form input[name=firstname]').val()) == ""){
                                errorMsg += "Please Enter Firstname \n";
                            }
                            if($.trim($('#co-shipping-form input[name=lastname]').val()) == ""){
                                errorMsg += "Please Enter Lastname \n";
                            }
                            if(telephoneval == "req"){
                                if($.trim($('#co-shipping-form input[name=telephone]').val()) == ""){
                                    errorMsg += "Please Enter Phone Number \n";
                                }
                            }
                            if($.trim($('#co-shipping-form select[name=country_id]').val()) == ""){
                                errorMsg += "Please Select Country \n";
                            }
                            if($.trim($('#co-shipping-form input[name=city]').val()) == ""){
                                errorMsg += "Please Enter City \n";
                            }
                            if($.trim($('#co-shipping-form input[name=postcode]').val()) == ""){
                                errorMsg += "Please Enter Postcode \n";
                            }
                            if($.trim($('#co-shipping-form input[name="street[0]"]').val()) == ""){
                                errorMsg += "Please Enter Street \n";
                            }
                            if($.trim(errorMsg) != ""){
                                //alert(errorMsg);
                                return;    
                            }
                        }
                        self.selectShipMethod();
                    }
                    $(".payment-method._active").find('.action.primary.checkout').trigger('click');
                    });
                });
            },
            selectShipMethod: function(){
                $('#co-shipping-method-form').submit();
                return;
            }
        });
    }
);