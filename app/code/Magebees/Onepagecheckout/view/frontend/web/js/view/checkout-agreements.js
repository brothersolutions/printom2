/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
define(
    [
        'ko',
        'jquery',
        'uiComponent',
        'Magebees_Onepagecheckout/js/model/agreements/agreements-modal',
        'Magebees_Onepagecheckout/js/magebees/plugins/jquery.nicescroll.min'
    ],
    function (ko, $, Component, agreementsModal) {
        'use strict';
        /* var agreementsConfig = window.checkoutConfig.checkoutAgreements,
            agreementManualMode = 1; */
		
		/* alert(agreementsConfig.agreements.toSource()); */
			
		/* alert(agreementsConfig.isEnabled);
		alert(window.checkoutConfig.enable_terms); */
		if(window.checkoutConfig.enable_terms == 1){
			var enable_terms = true;
		}else{
			var enable_terms = false;
		}
		
        return Component.extend({
            defaults: {
                template: 'Magebees_Onepagecheckout/checkout/checkout-agreements'
            },
            //isVisible: agreementsConfig.isEnabled,
			
            isVisible: enable_terms,
           /*  agreements: agreementsConfig.agreements, */
            checkboxTitle: ko.observable(window.checkoutConfig.term_checkboxtitle),
			modalTitle: ko.observable(null),
            modalContent: ko.observable(null),
            modalWindow: null,
			//termscontent: window.checkoutConfig.term_title,

            /**
             * Checks if agreement required
             *
             * @param element
             */
            isAgreementRequired: function(element) {
                //return element.mode == agreementManualMode;
				return element.mode == 1;
            },

            /**
             * Show agreement content in modal
             *
             * @param element
             */
            showContent: function (element) {
                this.modalTitle(window.checkoutConfig.term_title);
                this.modalContent(window.checkoutConfig.term_html);
                agreementsModal.showModal();
                $('.magebees-checkout-agreements-modal').closest('.modal-content').niceScroll({cursorcolor:"#e5e5e5",cursorwidth:"8px",railpadding: { top: 0, right: 7, left: 0, bottom: 0 }});
            },

            /**
             * Init modal window for rendered element
             *
             * @param element
             */
            initModal: function(element) {
                agreementsModal.createModal(element);
            }
        });
    }
);
