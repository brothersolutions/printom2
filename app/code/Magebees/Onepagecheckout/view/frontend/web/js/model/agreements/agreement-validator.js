/*jshint browser:true jquery:true*/
/*global alert*/
define(
    [
        'jquery',
        'mage/validation'
    ],
    function ($) {
        'use strict';
        var agreementsConfig = window.checkoutConfig.checkoutAgreements;
        return {
            /**
             * Validate checkout agreements
             *
             * @returns {boolean}
             */
            validate: function() {
                if (!agreementsConfig.isEnabled) {
                    return true;
                }

                var form = $('form#magebees-checkout-agreements-form');
                form.validation();
                return form.validation('isValid');
            }
        }
    }
);
