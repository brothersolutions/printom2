<?php
namespace Magebees\Onepagecheckout\Observer;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class CustomerLogin implements ObserverInterface
{
	
    protected $scopeConfig;
    protected $uri;
	protected $_magebeesConfigHelper;
    protected $responseFactory;

    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Zend\Validator\Uri $uri,
		\Magento\Framework\UrlInterface $urlinterface,
        \Magento\Framework\App\ResponseFactory $responseFactory,
		\Magebees\Onepagecheckout\Helper\Configurations $magebeesConfigHelper,
		 \Magento\Framework\App\RequestInterface $request
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->uri = $uri;
		$this->_urlinterface = $urlinterface;
		$this->_magebeesConfigHelper = $magebeesConfigHelper;
        $this->responseFactory = $responseFactory;
		$this->_request = $request;
    }

	public function execute(Observer $observer)
    {
		$current = $this->_request->getPost()->toarray();
		if(isset($current['magebees_ocp'])){
			//$enabled = $this->scopeConfig->getValue('magebees_Onepagecheckout/general/enable_in_frontend');
			$enabled = $this->_magebeesConfigHelper->getEnable();
			if ($enabled == 1) {
				if($current['magebees_ocp']){
					$resultRedirect = $this->responseFactory->create();
					$resultRedirect->setRedirect($this->_urlinterface->getUrl('checkout'))->sendResponse('200');
					//exit();
					return;
				}	
			}
		}
    }
}
