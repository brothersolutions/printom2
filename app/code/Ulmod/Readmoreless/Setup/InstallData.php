<?php
/**
 * Copyright © Ulmod. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Ulmod\Readmoreless\Setup;

use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Catalog\Model\Category as CategoryModel;
use Magento\Catalog\Model\Product as ProductModel;
use Magento\Catalog\Model\ResourceModel\Eav\Attribute;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;

class InstallData implements InstallDataInterface
{
    /**
     * EAV setup factory
     *
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * @param EavSetupFactory $eavSetupFactory
     */
    public function __construct(EavSetupFactory $eavSetupFactory)
    {
        $this->eavSetupFactory = $eavSetupFactory;
    }

    /**
     * {@inheritdoc}
     */	
    public function install(
        ModuleDataSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        /** @var EavSetup $eavSetup */
        $eavSetup = $this->eavSetupFactory->create(
			['setup' => $setup]
		);
		
         // Categoy Page
        $eavSetup->addAttribute(
            CategoryModel::ENTITY,
            'um_cat_desc_height',
            [
                'group'         => 'Read More Less (by Ulmod)',
                'type'          => 'varchar',
                'label'         => 'Description Height (px)',
                'note'          => 'Enter description height in pixel (This setting was added by Readmoreless by Ulmod)',					
                'input'         => 'text',
                'global'        => Attribute::SCOPE_STORE,
                'required'      => false,
                'user_defined'  => false,
                'default'       => '',	
                'sort_order'    => 200,
            ]
        );
        $eavSetup->addAttribute(
            CategoryModel::ENTITY,
            'um_cat_read_more_text',
            [
                'group'         => 'Read More Less (by Ulmod)',
                'type'          => 'varchar',
                'label'         => 'Read More Button Text',
                'note'      	=> 'Enter text to appear on read more link. (This setting was added by Readmoreless by Ulmod)',					
                'input'         => 'text',
                'global'        => Attribute::SCOPE_STORE,
                'required'      => false,
                'user_defined'  => false,
                'default'       => '',
                'sort_order'    => 201
            ]
        );		
        $eavSetup->addAttribute(
            CategoryModel::ENTITY,
            'um_cat_read_less_text',
            [
                'group'         => 'Read More Less (by Ulmod)',
                'type'          => 'varchar',
                'label'         => 'Read Less Button Text',
                'note'      	=> 'Enter text to appear on read less link. (This setting was added by Readmoreless by Ulmod)',					
                'input'         => 'text',
                'global'        => Attribute::SCOPE_STORE,
                'required'      => false,
                'user_defined'  => false,
                'default'       => '',
                'sort_order'    => 202
            ]
        );
		
		
        // Product Page
        $eavSetup->addAttribute(
            ProductModel::ENTITY,
            'um_prod_desc_height',
            [
                'group' => 'Read More Less (by Ulmod)',
                'type' => 'varchar',
                'backend' => '',
                'frontend' => '',
                'label' => 'Description Height (px)',
                'class' => '',
                'source' => '',
                'global' => ScopedAttributeInterface::SCOPE_STORE,
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'default' => '',
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => true,
                'unique' => false,
                'apply_to' => 'simple,configurable,virtual,bundle,downloadable,grouped',
                'system' => false
            ]
        );
        $eavSetup->addAttribute(
            ProductModel::ENTITY,
            'um_prod_read_more_text',
            [
                'group' => 'Read More Less (by Ulmod)',
                'type' => 'varchar',
                'backend' => '',
                'frontend' => '',
                'label' => 'Read More Button Text',			
                'class' => '',
                'source' => '',
                'global' => ScopedAttributeInterface::SCOPE_STORE,
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'default' => '',
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => true,
                'unique' => false,
                'apply_to' => 'simple,configurable,virtual,bundle,downloadable,grouped',
                'system' => false
            ]
        );
        $eavSetup->addAttribute(
            ProductModel::ENTITY,
            'um_prod_read_less_text',
            [
                'group' => 'Read More Less (by Ulmod)',
                'type' => 'varchar',
                'backend' => '',
                'frontend' => '',
                'label' => 'Read Less Button Text',			
                'class' => '',
                'source' => '',
                'global' => ScopedAttributeInterface::SCOPE_STORE,
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'default' => '',
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => true,
                'unique' => false,
                'apply_to' => 'simple,configurable,virtual,bundle,downloadable,grouped',
                'system' => false
            ]
        );		
    }
}
