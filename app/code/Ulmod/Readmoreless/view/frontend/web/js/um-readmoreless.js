/**
 * Copyright © Ulmod. All rights reserved.
 * See LICENSE.txt for license details.
 */ 
define([
    'jquery',
    'Ulmod_Readmoreless/js/lib/readmore'    
], function ($) {
    "use strict";
    return function (config) {
		$(config.moreLessSelector).readmore({
		  speed: 75,
		  collapsedHeight: config.moreLessDescHeight,	
		  heightMargin: 16,		  
		  embedCSS: false,
		  moreLink: '<a href="#" class="um-descr-show-button">' + config.moreLinkText + '</a>',		  
		  lessLink: '<a href="#" class="um-descr-hide-button">' + config.lessLinkText + '</a>',
          beforeToggle: function(trigger, element, expanded) {
            element.addClass('transitioning');
          },
          afterToggle: function(trigger, element, expanded) {
            element.removeClass('transitioning');
          }			  
		});		
    }
});
