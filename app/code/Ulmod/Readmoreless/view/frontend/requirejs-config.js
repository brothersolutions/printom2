/**
 * Copyright © Ulmod. All rights reserved.
 * See LICENSE.txt for license details.
 */
var config = {
    map: {
        '*': {
            umReadmoreless: 'Ulmod_Readmoreless/js/um-readmoreless'
        }
    }
};
